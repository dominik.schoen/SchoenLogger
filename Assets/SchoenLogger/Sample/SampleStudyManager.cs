﻿namespace SchoenLogger.Sample
{
    public class SampleStudyManager : StudyManager<SampleCondition>
    {
        protected override void CreateConditions(ref SampleCondition[] conditions)
        {
            conditions = new SampleCondition[2];
            conditions[0] = new SampleCondition(SampleCondition.MovementType.randomMovement);
            conditions[1] = new SampleCondition(SampleCondition.MovementType.fastRandomMovement);
        }
    }
}