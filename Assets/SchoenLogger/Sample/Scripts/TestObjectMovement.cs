using System.Collections;
using System.Collections.Generic;
using SchoenLogger;
using SchoenLogger.Sample;
using UnityEngine;

namespace SchoenLogger.Sample
{
    public class TestObjectMovement : MonoBehaviour
    {
        public StudyManager<SampleCondition> StudyManager;
        public float slowSpeed = 3;
        public float fastSpeed = 6;
        
        private SampleCondition currentCondition;
        private bool started = false;
        private Vector3 target = Vector3.zero;
        
        // Start is called before the first frame update
        void Start()
        {
            StudyManager.ChangeCondition.AddListener(SetupCondition);
            StudyManager.StartCondition.AddListener(StartExperiment);
        }

        // Update is called once per frame
        void Update()
        {
            if (currentCondition == null || !started)
                return;

            if (Vector3.SqrMagnitude(target - transform.position) < 0.01f)
            {
                target.x = Random.Range(-5, 5);
                target.y = Random.Range(-5, 5);
                target.z = Random.Range(-5, 5);
            }
            
            switch (currentCondition.movement)
            {
                case SampleCondition.MovementType.randomMovement:
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * slowSpeed);
                    break;
                case SampleCondition.MovementType.fastRandomMovement:
                    transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * fastSpeed);
                    break;
            }
        }

        private void StartExperiment(SampleCondition arg0, int arg1)
        {
            started = true;
        }

        void SetupCondition(SampleCondition cond, int partId)
        {
            currentCondition = cond;
        }
    }
}