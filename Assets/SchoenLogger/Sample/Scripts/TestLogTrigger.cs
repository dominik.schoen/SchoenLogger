using UnityEngine;

namespace SchoenLogger.Sample
{
    public class TestLogTrigger : MonoBehaviour
    {
        public Logger<SampleLogEntry, SampleCondition> Logger;
        
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        private void OnTriggerEnter(Collider other)
        {
            if (Logger == null)
                return;
            
            SampleLogEntry entry = new SampleLogEntry();
            entry.Time = Time.time;
            Vector3 point = other.ClosestPoint(this.transform.position);
            entry.EntryPointX = point.x * 20000;
            entry.EntryPointY = point.y / 20000;
            entry.EntryPointZ = point.z;
            Logger.Log(entry);
        }
    }

}