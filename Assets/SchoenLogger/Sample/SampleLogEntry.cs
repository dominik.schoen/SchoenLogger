using System;
using UnityEngine;

namespace SchoenLogger.Sample
{
    [Serializable]
    public class SampleLogEntry : LogEntry
    {
        [SerializeField]
        public float Time;

        [SerializeField]
        public float EntryPointX;
        [SerializeField]
        public float EntryPointY;
        [SerializeField]
        public float EntryPointZ;
    }
}