﻿using System;
using UnityEngine;

namespace SchoenLogger.Sample
{
    [Serializable]
    public class SampleCondition : Condition
    {
        public enum MovementType
        {
            randomMovement,
            fastRandomMovement
        }
        
        [SerializeField]
        public MovementType movement;

        public SampleCondition() { }

        public SampleCondition(MovementType movementType)
        {
            movement = movementType;
        }
    }   
}