using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace SchoenLogger
{
    public class CsvCompatible
    {
        const BindingFlags Bindings = BindingFlags.Public |
                                      BindingFlags.NonPublic |
                                      BindingFlags.Instance;
    
        public virtual string ToCsv()
        {
            StringBuilder csvString = new StringBuilder();

            FieldInfo[] fields = this.GetType()
                .GetFields(Bindings)
                .ToArray();

            foreach (FieldInfo fieldInfo in fields)
            {
                if (Attribute.IsDefined(fieldInfo, typeof(SerializeField)))
                {
                    if (fieldInfo.FieldType == typeof(float))
                    {
                        csvString.Append(";" + ((float)fieldInfo.GetValue(this)).ToString("G", CultureInfo.InvariantCulture));
                        continue;
                    }
                    
                    if (fieldInfo.FieldType == typeof(double))
                    {
                        csvString.Append(";" + ((double)fieldInfo.GetValue(this)).ToString("G", CultureInfo.InvariantCulture));
                        continue;
                    }
                    
                    csvString.Append(";" + fieldInfo.GetValue(this).ToString());
                }
            }
            
            return csvString.ToString();
        }

        public static string GetCsvHeader<T>()
        {
            StringBuilder headerString = new StringBuilder();

            FieldInfo[] fields = typeof(T)
                .GetFields(Bindings)
                .ToArray();

            foreach (FieldInfo fieldInfo in fields)
            {
                if (Attribute.IsDefined(fieldInfo, typeof(SerializeField)))
                    headerString.Append(";" + fieldInfo.Name);
            }
            
            return headerString.ToString();
        }
    }
}