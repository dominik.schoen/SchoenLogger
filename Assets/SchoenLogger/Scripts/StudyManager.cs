﻿using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SchoenLogger
{
    public abstract class StudyManager<TCondition> : MonoBehaviour, IStudyManager where TCondition : Condition, new()
    {
        [Header("Study Settings")]
        public int ParticipantId = -1;
        [Header("Study Events")]
        public UnityEvent<TCondition, int> ChangeCondition;
        public UnityEvent<TCondition, int> StartCondition;
        
        [Header("Misc")]
        [SerializeField]
        protected bool EnableConsoleLogging = false;
        protected TCondition[] Conditions;
        protected int CurrentConditionIndex = -1;
    
        // Start is called before the first frame update
        void Start()
        {
            CreateConditions(ref Conditions);
        }

        /// <summary>
        /// Creates all possible Conditions
        /// </summary>
        /// <param name="conditions"></param>
        protected abstract void CreateConditions(ref TCondition[] conditions);

        public void RaiseNextCondition()
        {
            CurrentConditionIndex++;
            if (CurrentConditionIndex >= Conditions.Length)
                return;

            ChangeCondition?.Invoke(Conditions[CurrentConditionIndex], ParticipantId);
            
            if(EnableConsoleLogging)
                Debug.LogFormat("Changed Condition to {0}!", CurrentConditionIndex);
        }

        public void RaiseStartCondition()
        {
            StartCondition?.Invoke(Conditions[CurrentConditionIndex], ParticipantId);
            
            if(EnableConsoleLogging)
                Debug.LogFormat("Started Condition! {0}", Conditions[CurrentConditionIndex].ToCsv());
        }

        public string GetConditionCountString()
        {
            if (Conditions == null || !Application.isPlaying)
                return "Only available on play";

            return Conditions.Length.ToString();
        }

        public int GetCurrentConditionIndex()
        {
            return CurrentConditionIndex;
        }
    }

    public interface IStudyManager
    {
        void RaiseNextCondition();

        void RaiseStartCondition();

        string GetConditionCountString();

        int GetCurrentConditionIndex();
    }
    
#if UNITY_EDITOR
    [CustomEditor(typeof(StudyManager<>), true)]
    public class StudyManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            IStudyManager Target = (IStudyManager)target;

            //EditorGUILayout.Space(10);
            //EditorGUILayout.LabelField("Manage Conditions", EditorStyles.boldLabel);
            EditorGUILayout.LabelField("Defined Conditions: ", Target.GetConditionCountString());
            EditorGUILayout.LabelField("Current Condition: ", Target.GetCurrentConditionIndex().ToString());
            EditorGUILayout.Space(5);
            EditorGUILayout.LabelField("Controlls", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Setup next Condition"))
            {
                Target.RaiseNextCondition();
            }
            if (GUILayout.Button("Setup & start next Condition"))
            {
                Target.RaiseNextCondition();
                Target.RaiseStartCondition();
            }
            GUILayout.EndHorizontal();
            if (GUILayout.Button("StartCondition"))
            {
                Target.RaiseStartCondition();
            }
        }
    }
#endif
}