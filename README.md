# Schoen Logger
Simple logger for user studies built with unity. Can be used to controll studies with simultaneous logging.

## Usage

### Quick Installation
Visit the [Release Page](https://git.tk.informatik.tu-darmstadt.de/dominik.schoen/SchoenLogger/releases) for the latest release and download the source zip file. Extract the content and copy the `/Assets/SchoenLogger` into your project's `/Assets` folder.

### Samples
Check out the `/SchoenLogger/Sample/LoggerSample.unity` scene for sample implementation.